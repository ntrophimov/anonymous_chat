require 'curses'
require 'securerandom'
require 'socket'
require 'time'

require './os'

# Unfortunately, we can't use the Curses::KEY_BACKSPACE
# See http://stackoverflow.com/questions/25402135/how-to-properly-work-with-curseskey-backspace-in-ruby for details
if OS.mac?
  $backspace_character = 127
elsif OS.linux?
  $backspace_character = 263
else
  $backspace_character = 8
end

$carriage_return_character = 13
$end_of_text_character = 3

def do_at_exit
  at_exit { yield }
end

def time_diff_milli(start, finish)
  (finish - start) * 1000.0
end

class ChatScreen
  def initialize
    @terminate_after_next_user_input = false

    @messages = []
    @sent_messages_history = []
    @max_sent_messages_history = 100
    @cur_sent_messages_history_index = -1

    Curses.init_screen
    # This must be called, in order to use color attributes
    # It is good practice to call it just after Curses.init_screen
    Curses.start_color
    # We need to disable the echo mode because we need to control which characters should be displayed
    # (for example, in case of window's size restrictions)
    Curses.noecho
    Curses.nonl

    Curses.init_pair(Curses::COLOR_YELLOW, Curses::COLOR_YELLOW, Curses::COLOR_BLACK)
    Curses.init_pair(Curses::COLOR_CYAN, Curses::COLOR_CYAN, Curses::COLOR_BLACK)
    Curses.init_pair(Curses::COLOR_RED, Curses::COLOR_RED, Curses::COLOR_BLACK)

    @win = Curses::Window.new(Curses.lines,
                              Curses.cols,
                              0,
                              0)
    @vertical_frame_character = ?|
    @horizontal_frame_character = ?-
    @win.box(@vertical_frame_character, @horizontal_frame_character)

    @win.keypad(true) # We need it to enable arrow keys

    @prompt_str = '> '
    @input_line_y = Curses.lines - 2
    @input_line_x = 1 + @prompt_str.length
    @win.setpos(@input_line_y, 1)
    @win.attron(Curses.color_pair(Curses::COLOR_YELLOW)) {
      @win.addstr(@prompt_str)
    }

    # - 3 because we need to exclude the window's borders and input line
    @lines_available_for_messages = Curses.lines - 3
    # - 2 because we need to exclude the window's borders
    @max_lenth_of_message_part = Curses.cols - 2

    @win.refresh

    do_at_exit { close }
  end

  def close
    if not @message_box.nil?
      @message_box.clear
      @message_box.refresh
      @message_box.close
    end
    if not @win.nil?
      @win.clear
      @win.refresh
      @win.close
    end
    Curses.close_screen
  end

  def get_user_input
    set_cursor_to_input_line
    clr_to_last_character
    # We can't set the max. length of the input string in case of getstr function,
    # so let's read characters one by one
    res = ''
    catch :done do loop {
      ch = @win.getch
      if @terminate_after_next_user_input
        Kernel::exit
      end
      case ch
      when Curses::KEY_UP
        if @cur_sent_messages_history_index + 1 < @sent_messages_history.length
          set_cursor_to_input_line
          clr_to_last_character
          @cur_sent_messages_history_index += 1
          msg = @sent_messages_history[@cur_sent_messages_history_index].dup
          @win.addstr(msg)
          res = msg
        end
      when Curses::KEY_DOWN
        if @cur_sent_messages_history_index - 1 >= 0
          set_cursor_to_input_line
          clr_to_last_character
          @cur_sent_messages_history_index -= 1
          msg = @sent_messages_history[@cur_sent_messages_history_index].dup
          @win.addstr(msg)
          res = msg
        end
      when Curses::KEY_LEFT, Curses::KEY_RIGHT
        # We don't need to handle these keys
      when $end_of_text_character
        Kernel::exit
      when $carriage_return_character
        throw :done if not res.strip.empty?
      when $backspace_character
        # Prevent prompt character from deletion
        if @win.curx > @input_line_x
          # "noecho" mode disables even backspace characters handling, so let's do it manually
          # Move the cursor one character back
          @win.setpos(@win.cury, @win.curx - 1)
          # Also, just move the cursor position is not enough -- we need to delete the character in the current position too
          # @win.delch will move the last character (in this case it's a pipe character of the window's box),
          # so let's clear the entire line from the current coordinates and print the last character manually
          # We can't use arrows, so this behavior looks right
          clr_to_last_character
          res.chop!
        end
      else
        if @win.curx < Curses.cols - 1
          @win.addch(ch)
          res << ch
        end
      end
    }
    end

    @sent_messages_history.pop if @sent_messages_history.length > @max_sent_messages_history
    @sent_messages_history.unshift(res)
    @cur_sent_messages_history_index = -1

    res
  end

  def add_new_message(message)
    @messages << message
    draw_messages
  end

  def show_message(*msgs)
    message = msgs.join
    width = message.length + 6
    @message_box = Curses::Window.new(
      5,
      width,
      (Curses.lines - 5) / 2,
      (Curses.cols - width) / 2)
    @message_box.attron(Curses.color_pair(Curses::COLOR_RED)) {
      @message_box.box(?|, ?-)
    }
    @message_box.setpos(2, 3)
    @message_box.addstr(message)
    @message_box.refresh
  end

  def set_cursor_to_input_line
    @win.setpos(@input_line_y, @input_line_x)
  end

  def draw_messages
    prev_x = @win.curx
    prev_y = @win.cury

    messages_parts_to_print = []
    catch :done do
      @messages.last(@lines_available_for_messages).reverse.each do |message|
        message_parts = message.chars.each_slice(@max_lenth_of_message_part).map(&:join)
        message_parts.reverse.each do |message_part|
          messages_parts_to_print.unshift(message_part)
          throw :done if messages_parts_to_print.length == @lines_available_for_messages
        end
      end
    end

    # 1 because we need to skip the top border of the window
    messages_parts_to_print.each.inject(1) do |line_number, message_part|
      # 1 because we need to skip the left border of the window
      @win.setpos(line_number, 1)
      clr_to_last_character

      # If the message starts with the [.+] substring, color it
      if message_part =~ /\A(\[.+?\])(.+)/
        @win.attron(Curses.color_pair(Curses::COLOR_CYAN)) {
          @win.addstr("#{Regexp.last_match[1]}")
        }
        @win.addstr("#{Regexp.last_match[2]}")
      else
        @win.addstr("#{message_part}")
      end
      line_number + 1
    end

    @win.setpos(prev_y, prev_x)
    @win.refresh
  end

  def clr_to_last_character
    # Actually we're clear the entire line from the current position to eol and redraw the last character manually
    prev_x = @win.curx
    prev_y = @win.cury
    @win.clrtoeol
    @win.setpos(prev_y, Curses.cols - 1)
    @win.addch(@vertical_frame_character)
    @win.setpos(prev_y, prev_x)
  end

  attr_accessor :terminate_after_next_user_input
  private :set_cursor_to_input_line, :draw_messages, :clr_to_last_character
end

class Client
  def initialize(server)
    @pings_send_time = {}
    @chat_screen = ChatScreen.new
    @server = server
    @request = nil
    @response = nil
    listen
    send
    @request.join
    @response.join
  end

  def listen
    @response = Thread.new do
      loop {
        begin
          msg = @server.gets.chomp
        rescue
          @chat_screen.show_message('You\'ve been disconnected from the server. Press any key to exit')
          # Kernel::exit call won't terminate the whole application because Curses.getch function in the delay mode
          # will wait for the next user input anyway, unlike gets.chomp, for example
          # It's ok in this case because we want to give the user the ability to see the message box before
          # program termination, so let's mark it in the appropriate way and wait for the next user input
          # See https://www.ruby-forum.com/topic/5437509 for details
          @chat_screen.terminate_after_next_user_input = true
        end

        if msg.start_with?('[SERVER]')
          handle_ingoing_server_message(msg)
        else
          handle_ingoing_user_message(msg)
        end
      }
    end
  end

  def send
    @request = Thread.new do
      loop {
        msg = @chat_screen.get_user_input
        if msg.start_with?('/')
          handle_outgoing_user_command(msg)
        else
          handle_outgoing_regular_message(msg)
        end
      }
    end
  end

  def handle_outgoing_user_command(command)
    if command == '/ping'
      # When the user tries to send a ping message, we're add the UUID to the end of the command
      # to identify it when we receive the pong response from the server
      ping_id = SecureRandom.uuid
      @pings_send_time[ping_id] = Time.now
      @server.puts("#{command}:#{ping_id}")
    else
      @server.puts(command)
    end
  end

  def handle_outgoing_regular_message(message)
    @server.puts(message)
  end

  def handle_ingoing_server_message(message)
    if message.start_with?('[SERVER] pong')
      # Format: "[SERVER] pong:2d931510-d99f-494a-8c67-87feb05e1594"
      pong_command_parts = message.split(':')
      if pong_command_parts.length == 2
        ping_id = pong_command_parts[1]
        ping_send_time = @pings_send_time[ping_id]
        ping_result = time_diff_milli(ping_send_time, Time.now)
        @pings_send_time.delete(ping_id)
        @chat_screen.add_new_message("[SERVER] pong: #{ping_result} ms")
      end
    else
      @chat_screen.add_new_message(message)
    end
  end

  def handle_ingoing_user_message(message)
    @chat_screen.add_new_message(message)
  end

  private :listen,
          :send,
          :handle_outgoing_user_command,
          :handle_outgoing_regular_message,
          :handle_ingoing_server_message,
          :handle_ingoing_user_message
end

if not defined?(Ocra)
  if ARGV.length != 2
    abort 'Usage: client [ip_address] [port]'
  end

  puts 'Connecting...'
  server = TCPSocket.new ARGV[0], ARGV[1]
  Client.new(server)
end
