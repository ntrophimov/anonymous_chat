What is this?
=============

anonymous_chat is a fully anonymous desktop messenger

How can I help?
=============

Feel free to do any pull requests and fork it as much as you wish. Also don't forget to report any bugs you found!
