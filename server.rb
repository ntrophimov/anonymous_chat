require 'date'
require 'logger'
require 'socket'

$logger = Logger.new('server.log', 'daily')

class Server
  def initialize(port)
    @server = TCPServer.open('0.0.0.0', port)
    @clients = []
    run
  end

  def run
    $logger.info 'Server created'
    loop {
      Thread.start(@server.accept) do |client|
        @clients << client
        $logger.info "client '#{client}' connected"
        client.puts '[SERVER] Connection established'
        listen_user_messages(client)
      end
    }
  end

  def listen_user_messages(client)
    loop {
      begin
        msg = client.gets.chomp
      rescue
        @clients.delete(client)
        $logger.info "client '#{client}' disconnected"
        return
      end

      $logger.info "client '#{client}' said: #{msg}"
      if msg.start_with?('/')
        handle_user_command(client, msg)
      else
        handle_regular_message(msg)
      end
    }
  end

  def handle_user_command(client, command)
    now = Time.now.strftime('%d/%m/%Y %H:%M')
    if command.casecmp('/time') == 0
      client.puts "[SERVER] Current time is: #{now}"
    elsif command.casecmp('/users') == 0
      client.puts("[SERVER] There are #{@clients.length} user(s) now")
    elsif command.downcase.start_with?('/ping')
      # Format: "/ping:2d931510-d99f-494a-8c67-87feb05e1594"
      ping_command_parts = command.split(':')
      if ping_command_parts.length == 2
        ping_id = ping_command_parts[1]
        client.puts("[SERVER] pong:#{ping_id}")
      end
    else
      client.puts('[SERVER] Unknown command')
    end
  end

  def handle_regular_message(message)
    now = Time.now.strftime('%d/%m/%Y %H:%M')
    @clients.each do |some_client|
      some_client.puts "[#{now}]: #{message}"
    end
  end

  private :run, :listen_user_messages, :handle_user_command, :handle_regular_message
end

if not defined?(Ocra)
  if ARGV.length != 1
    abort 'Usage: server [port]'
  end

  Server.new(ARGV[0])
end
